## [1.0.8] - 2019-05-03
### Added
- layout syntax fixed.

## [1.0.7] - 2019-05-02
### Added
- buttonIcon syntax fixed.
- treeGrid syntax fixed.

## [Unreleased]

- Initial release